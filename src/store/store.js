import Vuex from "vuex";
import Vue from "vue";
import {booksService} from "../services/booksService"

Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        books: {map: new Map(), bookList: [], searchParams: {filter: "", sort: "", page: 1}},
        authors: {map: new Map(), authorList: []},
        publishing: {map: new Map(), publishingList: []},
    },
    mutations: {
        SET_BOOKS(state, books) {
            state.books.map = new Map(state.books.map);
            for (let book of books) {
                state.books.map.set(book.id, book);
            }
            state.books.bookList = books.map(item => item.id);

        },
        SET_BOOK(state, book) {
            state.books.map.set(book.id, book)
        }
    },
    actions: {
        async fetchBooks({commit}) {
            commit('SET_BOOKS', await booksService.fetchBooks());
        },
        async fetchBook({commit}, id) {
            commit('SET_BOOK', await booksService.fetchBook(id))
        }
    },
    getters: {
        books: (state) => {
            return state.books;
        },
        getBook: state => id => {
            window.console.log("В сторе id", id);
            window.console.log("В сторе book", state.books.map.get(id));
            return state.books.map.get(id);
        }
    }
});