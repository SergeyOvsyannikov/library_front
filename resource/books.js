export const books = [
    {
        id: 1,
        name: 'Моя дивергенция',
        author: 'В. Фридман',
        publishing: 'ArsisBooks',
        isbn:'9785904155926'
    },
    {
        id: 2,
        name: 'Паразит',
        author: 'Х. Ивааки',
        publishing: 'Alt Graph',
        isbn:'9785905295591'
    },
    {
        id: 3,
        name: 'Мания величия Человека-Паука',
        author: 'П. Бегги',
        publishing: 'Комильфо',
        isbn:'9785041196908'
    },
    {
        id: 4,
        name: 'Волшебные истории',
        author: 'А. Устинова',
        publishing: 'Эксмо',
        isbn:'9785041096908'
    },
    {
        id: 5,
        name: 'Крысиный король',
        author: 'Д. Стахов',
        publishing: 'ArsisBooks',
        isbn:'9785904155919'
    },
    {
        id: 6,
        name: 'Латгальский крест',
        author: 'В. Бочков',
        publishing: 'ArsisBooks',
        isbn:'9785904155933'
    },
    {
        id: 7,
        name: 'Ловчий',
        author: 'А. Башкуев',
        publishing: 'Реноме',
        isbn:'9785001252450'
    },
    {
        id: 8,
        name: 'Похищение Европы',
        author: 'Т. Беспалова',
        publishing: 'ArsisBooks',
        isbn:'9785904155810'
    },
    {
        id: 9,
        name: 'Самшитовый лес',
        author: 'М. Анчаров',
        publishing: 'Азбука',
        isbn:'9785389173675'
    },
    {
        id: 10,
        name: 'Второй взгляд',
        author: 'Дж. Пиколт',
        publishing: 'Азбука',
        isbn:'9785389159624'
    }
];